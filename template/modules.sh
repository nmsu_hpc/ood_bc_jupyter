#!/usr/bin/env bash

# Load Initial Modules
# shellcheck disable=SC2086
module load $OOD_MODULES

# Report Modules
echo ""
echo "The Follow Modules Are Loaded:"
module --terse list
echo ""

# Setup Command Replacements
if [ "$OOD_JINTERFACE" == "notebook" ]; then
	ood_jp () {
		if command -v jupyter &>/dev/null ; then
			jupyter notebook "$@"
		else
			echo "'jupyter' not in path! Stopping!!"
			exit 22
		fi
	}
	export -f ood_jp
else
	ood_jp () {
		if command -v jupyter &>/dev/null ; then
			jupyter-lab "$@"
		else
			echo "'jupyter-lab' not in path! Stopping!!"
			exit 22
		fi
	}
	export -f ood_jp
fi
