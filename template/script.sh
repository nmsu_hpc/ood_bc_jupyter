#!/usr/bin/env bash

# Ensure modules are set to default
module -q reset

# Prepare Jupyter based on submit type
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]]; then
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/sif.sh"
else
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/modules.sh"
fi

# Create Config File
OOD_JCONF="${PWD}/config.py"
export OOD_JCONF
(
umask 077
cat > "${OOD_JCONF}" << EOL
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.port = ${PORT}
c.NotebookApp.port_retries = 0
c.NotebookApp.password = u'sha1:${SALT}:${PASSWORD_SHA1}'
c.NotebookApp.base_url = '/node/${HOST}/${PORT}/'
c.NotebookApp.open_browser = False
c.NotebookApp.allow_origin = '*'
c.NotebookApp.notebook_dir = '${OOD_WD}'
c.NotebookApp.disable_check_xsrf = True
EOL
)

# CD Into Working Directory
cd "$OOD_WD" || true

# Source User's Environment Setup Secript
if [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Starting: Sourcing User Environment Setup Script"
	# shellcheck disable=SC1090
	source "$OOD_ENV_SETUP_SCRIPT"
	echo "Finished: Sourcing User Environment Setup Script"
elif [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ ! -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Error: User environment setup script '$OOD_ENV_SETUP_SCRIPT' specified but does not exist! Skipping!!"
fi

# Start VSCode
echo -e "\nStarting Jupyter:"
ood_jp --config "$OOD_JCONF"
