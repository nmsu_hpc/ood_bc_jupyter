#!/usr/bin/env bash

# Report SIF Image
echo ""
echo "The Following SIF Image Is Selected:"
echo "$OOD_SIF"
echo ""

# Setup Command Replacements
if [ "$OOD_JINTERFACE" == "notebook" ]; then
	ood_jp () {
		if [ -z "$OOD_SIF" ]; then
			echo 'Error: ENV "OOD_SIF" is not set! Stopping!!'
			exit 15
		elif [ ! -f "$OOD_SIF" ]; then
			echo "Error: '$OOD_SIF' does not exist! Stopping!!"
			exit 16
		elif ! apptainer exec "$OOD_SIF" bash -c "command -v jupyter &>/dev/null"; then
			echo "Error: 'jupyter' is missing from the PATH inside the provided container! Stopping!!"
			exit 17
		elif [ -n "$CUDA_VISIBLE_DEVICES" ]; then
			apptainer exec --nv "$OOD_SIF" jupyter notebook "$@"
		else
			apptainer exec "$OOD_SIF" jupyter notebook "$@"
		fi
	}
	export -f ood_jp
else
	ood_jp () {
		if [ -z "$OOD_SIF" ]; then
			echo 'Error: ENV "OOD_SIF" is not set! Stopping!!'
			exit 15
		elif [ ! -f "$OOD_SIF" ]; then
			echo "Error: '$OOD_SIF' does not exist! Stopping!!"
			exit 16
		elif ! apptainer exec "$OOD_SIF" bash -c "command -v jupyter-lab &>/dev/null"; then
			echo "Error: 'jupyter-lab' is missing from the PATH inside the provided container! Stopping!!"
			exit 17
		elif [ -n "$CUDA_VISIBLE_DEVICES" ]; then
			apptainer exec --nv "$OOD_SIF" jupyter-lab "$@"
		else
			apptainer exec "$OOD_SIF" jupyter-lab "$@"
		fi
	}
	export -f ood_jp
fi
