# Batch Connect - OSC Jupyter

![GitHub Release](https://img.shields.io/github/release/osc/bc_osc_jupyter.svg)
[![GitHub License](https://img.shields.io/badge/license-MIT-green.svg)](https://opensource.org/licenses/MIT)

An interactive app designed for OSC OnDemand that launches a Jupyter
server within an Owens batch job.

## Prerequisites

This Batch Connect app requires the following software be installed on the
**compute nodes** that the batch job is intended to run on (**NOT** the
OnDemand node):

- [Lmod] 6.0.1+ or any other `module purge` and `module load <modules>` based
  CLI used to load appropriate environments within the batch job before
  launching the Jupyter server.
- [Jupyter] 4.2.3+ (earlier versions are untested but may work for
  you)
- [OpenSSL] 1.0.1+ (used to hash the Jupyter server password)

[Jupyter]: https://jupyter.org/
[OpenSSL]: https://www.openssl.org/
[Lmod]: https://www.tacc.utexas.edu/research-development/tacc-projects/lmod

## Site Customizations

- `form.js`
  - Optional and can be removed.
- `form.yml.erb`
  - Values need to be updated to reflect your site.
  - The dynamic partition list may or maynot work for you.
- `partitions.json`
  - Partition values for dynamic partition list in form.yml.erb -> form 'custom_queue'.
- `before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.
- `modules.sh`
  - You'll need to adjust what modules are loaded.
- `sif.sh`
  - We install apptainer globally but if you need to load modules you can add that here.
- `manifest.yml`
  - links to documentation.
- `submit.yml.erb`
  - Adjujst parameters to match the scheduler/settings at your site.
